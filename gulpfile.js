var gulp = require('gulp')
var gulpSass = require('gulp-sass')
var gulpCopy = require('gulp-copy')

function sass() {
    return gulp.src('sass/main.scss')
        .pipe(gulpSass())
        .pipe(gulp.dest('dist/css'))
}

function copy() {
    return gulp.src([
            'js/**/*',
            'fonts/*',
            'img/*',
            'languages/*',
            '*.php',
            '*.css',
            '*.md',
            '*.png'
        ])
        .pipe(gulpCopy('dist/'))
}

var build = gulp.series(sass, copy)

exports.default = build;